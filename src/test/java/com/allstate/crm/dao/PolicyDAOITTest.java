package com.allstate.crm.dao;

import com.allstate.crm.entities.DatabaseSequence;
import com.allstate.crm.entities.Policy;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PolicyDAOITTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private PolicyDAO policyDao;

    @BeforeEach
    public void cleanupAndReset() {
        mongoTemplate.dropCollection(Policy.class);
        mongoTemplate.findAndModify(Query.query( Criteria.where("_id").is(Policy.SEQUENCE_NAME)),
                new Update().inc("seq",2), FindAndModifyOptions.options().returnNew(true).upsert(true),
                DatabaseSequence.class);


        mongoTemplate.insert(new Policy(
                1,
                21,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                ""
        ));
        mongoTemplate.insert(new Policy(
                2,
                21,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Terminated",
                new Date(),
                "Premium Due not cleared."
        ));

    }

    @Test
    public void findByTypeReturnsAllMatchingPayments() {
        List<Policy> policyDetails = policyDao.getPolicyByCustomerId(21);
        assertEquals(2, policyDetails.size());
    }

    @Test
    public void savePolicySucess()
    {
        Policy policy = new Policy(
                0,
                90,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                ""
        );
        Policy policy1 = new Policy(
                0,
                90,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Cancelld",
                new Date(),
                "not renewed the policy"
        );

        List<Policy> policies = new ArrayList<>();
        policies.add(policy);
        policies.add(policy1);
        policyDao.savePolicy(policies);
        assertEquals(2, policyDao.getPolicyByCustomerId(90).size());
    }

}
