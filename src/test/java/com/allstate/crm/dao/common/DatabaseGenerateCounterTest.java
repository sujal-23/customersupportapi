package com.allstate.crm.dao.common;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.DatabaseSequence;
import com.allstate.crm.entities.Policy;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class DatabaseGenerateCounterTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private DatabaseGenerateCounter dbSequence;

    @Mock
    private DatabaseGenerateCounter dbMockSequence;

    @BeforeEach
    public void cleanupAndReset() {
        mongoTemplate.dropCollection(DatabaseSequence.class);
    }

    @Test
    public void generateSequenceSuccess()
    {
        String sequence = Customer.SEQUENCE_NAME;
        long counter = dbSequence.generateSequence(sequence);
        assertTrue(counter >= 1 );
    }

    @Test
    public void generateSequencePolicySuccess()
    {
        String sequence = Policy.SEQUENCE_NAME;
        long counter = dbSequence.generateSequence(sequence);
        assertTrue( counter >= 1);
    }

    @Test
    public void updateSequenceSuccess()
    {
        String sequence = Customer.SEQUENCE_NAME;
        long counter = 1;
        doReturn(counter).when(dbMockSequence).updateSequence(1,sequence);
        assertTrue(counter == 1);
    }
}
