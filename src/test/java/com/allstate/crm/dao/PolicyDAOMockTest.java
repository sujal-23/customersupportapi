package com.allstate.crm.dao;

import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class PolicyDAOMockTest {

    @Mock
    private PolicyDAO dao;

    private Policy policyEntity;

    private AutoCloseable closeable;

    @Mock
    private DatabaseGenerateCounter dbCounter;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        policyEntity = new Policy(
                1,
                21,
                "9876541344",
                3287.47,
                 new Date(),
                "Auto",
                "Allstate",
                "Active",
                 null,
                 null
        );
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    void getPolicyByCustIdSuccess(){
        List<Policy> lstPolicy = new ArrayList<>();
        lstPolicy.add(policyEntity);
        doReturn(lstPolicy).when(dao).getPolicyByCustomerId(anyInt());
        assertEquals(policyEntity.getId(), lstPolicy.get(0).getId());
        assertEquals(policyEntity.getCustId(), lstPolicy.get(0).getCustId());
        assertEquals(policyEntity.getPremiumAmount(), lstPolicy.get(0).getPremiumAmount());
        assertEquals(policyEntity.getCompany(), lstPolicy.get(0).getCompany());
        assertEquals(policyEntity.getProductType(), lstPolicy.get(0).getProductType());
        assertEquals(policyEntity.getTerminationDate(), lstPolicy.get(0).getTerminationDate());
        assertEquals(policyEntity.getEffectiveDate(), lstPolicy.get(0).getEffectiveDate());
        assertEquals(policyEntity.getStatus(), lstPolicy.get(0).getStatus());
        assertEquals(policyEntity.getTerminationReason(), lstPolicy.get(0).getTerminationReason());
    }

    @Test
    void getPolicyByCustIdFailure(){
        List<Policy> policyInfo = new ArrayList<>();
        doReturn(policyInfo).when(dao).getPolicyByCustomerId(0);
        assertEquals(policyInfo.size(), 0);
    }

    @Test
    void savePolicySuccess(){
        List<Policy> lstPolicy = new ArrayList<>();
        lstPolicy.add(policyEntity);
        doReturn(1L).when(dbCounter).generateSequence(anyString());
        doReturn(1L).when(dbCounter).updateSequence(anyLong(),anyString());
        doNothing().when(dao).savePolicy(anyList());
        dao.savePolicy(lstPolicy);
        assertEquals(1L, dbCounter.generateSequence(Policy.SEQUENCE_NAME));
        assertEquals(1L, dbCounter.updateSequence(1L, Policy.SEQUENCE_NAME));
    }

    @Test
    void updatePolicySuccess(){
        List<Policy> lstPolicy = new ArrayList<>();
        lstPolicy.add(policyEntity);
        doReturn(1).when(dao).updatePolicy(anyList());
        assertEquals(1, dao.updatePolicy(lstPolicy));
    }
}
