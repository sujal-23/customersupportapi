package com.allstate.crm.dao;

import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class InteractionDAOMockTest {

    @Mock
    private InteractionDAO interactionDao;

    private Interaction interactionEntity;

    private AutoCloseable closeable;

    @Mock
    private DatabaseGenerateCounter dbCounter;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        interactionEntity = new Interaction(
                0,
                0,
                "test Interactions",
                "(541) 754-3010",
                "987654134",
                "sujal",
                new Date());
    }
    @Test
    void saveInteractionSuccess(){
        doReturn(1L).when(dbCounter).generateSequence(anyString());
        doReturn(1L).when(interactionDao).saveInteraction(any(Interaction.class));
        assertEquals(1L, interactionDao.saveInteraction(interactionEntity));
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
