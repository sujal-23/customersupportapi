package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDAO;
import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.exception.PolicyNotFoundException;
import com.allstate.crm.exception.PolicyServiceException;
import com.allstate.crm.utility.ValidationHelper;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class PolicyServiceMockTest {

    @InjectMocks
    PolicyService policyService = new PolicyServiceImpl();

    @Mock
    PolicyDAO policyDAO;

    @Mock
    CustomerDAO custDao;

    @Mock
    InteractionDAO interactionDao;

    @Mock
    ValidationHelper validation;

    @Mock
    AuditInteractionService audit;


    private AutoCloseable closeable;
    private Policy policyEntity;
    private Customer custEntity;




    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);

        custEntity = new  Customer(
                21,
                "kkkfff",
                "kkkfff",
                20,
                "Wheeling 123 789",
                "Chicago",
                "IL",
                "(234)-865-733",
                "test@test.com",
                "M",
                "7463663258",
                "English");
        policyEntity = new Policy(
                1,
                21,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );
    }

    @Test
    public void getPolicyByCustomerIdSuccess()
    {
        List<Policy> lstPolicy = new ArrayList<>();
        lstPolicy.add(policyEntity);
        doReturn(custEntity).when(custDao).getCustomerById(anyLong());
        doReturn(1L).when(interactionDao).saveInteraction(any(Interaction.class));
        doReturn(lstPolicy).when(policyDAO).getPolicyByCustomerId(anyInt());
        doReturn(true).when(validation).validateIntegerGreaterThanZero(anyInt());
        doReturn(1L).when(audit).auditInteractions(any(Customer.class), anyString(), anyString(), anyString());
        assertEquals(1L, audit.auditInteractions(custEntity, "View", "", "English"));
        assertEquals(1, policyService.getPolicyDetailByCustId(21).size());
        assertEquals(policyEntity.getId(), lstPolicy.get(0).getId());
        assertEquals(policyEntity.getPremiumAmount(), lstPolicy.get(0).getPremiumAmount());
        assertEquals(policyEntity.getCompany(), lstPolicy.get(0).getCompany());
    }

@Test
    public void getPolicyByCustomerIdFailure()
    {
        doReturn(false).when(validation).validateIntegerGreaterThanZero(0);
        PolicyServiceException exception = assertThrows(PolicyServiceException.class, () -> {
            policyService.getPolicyDetailByCustId(0);
        });

        String expectedMessage = "Policy Information not found.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Ignore
    public void getPolicyByCustomerIdFailureTypePolicyService()
    {
        doReturn(true).when(validation).validateIntegerGreaterThanZero(0);
        doThrow(new RuntimeException()).when(policyDAO).getPolicyByCustomerId(anyInt());
        PolicyNotFoundException exception = assertThrows(PolicyNotFoundException.class, () -> {
            policyService.getPolicyDetailByCustId(0);
        });

        String expectedMessage = "Error occured while processing your request.";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
