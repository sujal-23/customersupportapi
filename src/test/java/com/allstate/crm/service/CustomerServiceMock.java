package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDAO;
import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceMock {

    @InjectMocks
    private CustomerService custService = new CustomerServiceImpl();

    @Mock
    private DatabaseGenerateCounter dbCounter;

    @Mock
    private CustomerDAO custDao;

    @Mock
    private PolicyDAO policyDao;

    @Mock
    private InteractionDAO interactionDao;

    @Mock
    private  AuditInteractionService auditService;

    private AutoCloseable closeable;
    private Policy policyEntity;
    private Policy policyEntityNew;
    private Interaction interactions;
    private Customer custEntitiyNew;
    private Customer custEntitiy;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        custEntitiyNew = new  Customer(
        0,
        "zzzz",
        "zzzz",
        20,
        "Wheeling 123 789",
        "Chicago",
        "IL",
        "23498657",
        "test@test.com",
        "M",
        "7463663258",
        "English");
        custEntitiy = new  Customer(
                1,
                "kkk",
                "kkk",
                20,
                "Wheeling 123 789",
                "Chicago",
                "IL",
                "23498657",
                "test@test.com",
                "M",
                "7463663258",
                "English");

        policyEntity = new Policy(
                1,
                1,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );

        policyEntityNew = new Policy(
                0,
                0,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );

        interactions = new Interaction(
                0,
            31,
            "test Interactions",
            "3252672",
            "9876541344",
            "sujal",
             new Date()
        );
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }


    @Test
    public void saveCustomerDetails_Create_Success()
    {
        List<Policy> policies = new ArrayList<>();
        //policies.add(policyEntity);
        policies.add(policyEntityNew);
        doReturn(10L).when(dbCounter).generateSequence(anyString());
        doReturn(1L).when(dbCounter).updateSequence(anyLong(), anyString());
        doReturn(11L).when(custDao).saveCustomer(any(Customer.class));
        doReturn(1L).when(interactionDao).saveInteraction(any(Interaction.class));
        when(auditService.auditInteractions(
                any(Customer.class),
                anyString(),
                anyString(),
                anyString())).thenReturn(1L);
        doNothing().when(policyDao).savePolicy(anyList());
        doReturn(1).when(policyDao).updatePolicy(anyList());
        assertTrue( custService.saveCustomerInfo(custEntitiyNew, policies, interactions) > 0);
    }

    @Test
    public void saveCustomerDetails_Update_Success()
    {
        List<Policy> policies = new ArrayList<>();
        policies.add(policyEntity);
        //policies.add(policyEntityNew);
        doReturn(10L).when(dbCounter).generateSequence(anyString());
        doReturn(1L).when(dbCounter).updateSequence(anyLong(), anyString());
        doReturn(11L).when(custDao).updateCustomer(any(Customer.class));
        doReturn(1L).when(interactionDao).saveInteraction(any(Interaction.class));
        when(auditService.auditInteractions(
                any(Customer.class),
                anyString(),
                anyString(),
                anyString())).thenReturn(1L);
        doNothing().when(policyDao).savePolicy(anyList());
        doReturn(1).when(policyDao).updatePolicy(anyList());
        assertTrue( custService.saveCustomerInfo(custEntitiy, policies, interactions) > 0);
    }
}
