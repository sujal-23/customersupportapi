package com.allstate.crm.rest;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.CustomerDetails;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;


@AutoConfigureMockMvc
@SpringBootTest()
@Ignore
public class CustomerControllerTest {

    private int port=8080;
    private AutoCloseable closeable;


    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    Policy policyEntity;
    Customer customer;
    Interaction interaction;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        policyEntity = new Policy(
                0,
                0,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );
        customer = new Customer(
                0,
                "kkk",
                "kkk",
                20,
                "Wheeling 123 789",
                "Chicago",
                "IL",
                "(541) 754-3010",
                "test@test.com",
                "M",
                "856-45-6789",
                "English");

       interaction = new Interaction(
                0,
                0,
                "test Interactions",
                "(541) 754-3010",
                "987654134",
                "sujal",
                new Date());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Ignore
    public void saveCustomerSuccess() throws JSONException {
        List<Policy> policies = new ArrayList<>();
        policies.add(policyEntity);
        HttpEntity<CustomerDetails> entity = new HttpEntity<CustomerDetails>(new CustomerDetails(
             customer, policies, interaction ), headers);
        ResponseEntity<Long> response = restTemplate.exchange(createURLWithPort("api/customer/save/"),
                HttpMethod.POST, entity ,Long.TYPE
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }

}
