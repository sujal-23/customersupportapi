package com.allstate.crm.rest;

import com.allstate.crm.entities.Policy;
import org.json.JSONException;
import org.junit.After;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest()
@Ignore
public class PolicyControllerTest {

    private int port=8080;
    private AutoCloseable closeable;

    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    Policy policyEntity;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        policyEntity = new Policy(
                1,
                21,
                "9876541344",
                3287.47,
                new Date(),
                "Auto",
                "Allstate",
                "Active",
                null,
                null
        );
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Ignore
    public void getPolicyByCustomerId() throws JSONException {
        HttpEntity<Integer> entity = new HttpEntity<Integer>(21, headers);

        ResponseEntity<Policy[]> response = restTemplate.exchange(createURLWithPort("api/customer/policy/21"),
                HttpMethod.GET, entity, Policy[].class , 21
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
