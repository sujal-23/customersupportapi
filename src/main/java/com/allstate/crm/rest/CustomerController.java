package com.allstate.crm.rest;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.CustomerDetails;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import javax.validation.Valid;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class CustomerController {

    Logger logger = LoggerFactory.getLogger(CustomerController.class);
    @Autowired
    private CustomerService custService;

    @RequestMapping(value = "customer/save", method = RequestMethod.POST)
    public void save(@Valid @RequestBody CustomerDetails customer)
    {
       long retunVal = custService.saveCustomerInfo(customer.getCustomer(), customer.getPolicies(),
               customer.getInteractons());
    }
}
