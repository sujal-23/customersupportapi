package com.allstate.crm.rest;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.service.PolicyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class PolicyController {

    Logger logger = LoggerFactory.getLogger(PolicyController.class);

    @Autowired
    private PolicyService service;

   public PolicyController()
    {

    }

    @RequestMapping(value = "/customer/policy/{custId}", method = RequestMethod.GET)
    public List<Policy> getPolicyByCustId(@PathVariable("custId") int custId)
    {
        logger.info("starting method getPolicyByCustId  " + custId);
        List<Policy> policyDetails;
        policyDetails = service.getPolicyDetailByCustId(custId);
        logger.info("policy details fetched", policyDetails);

        return policyDetails;
    }
}
