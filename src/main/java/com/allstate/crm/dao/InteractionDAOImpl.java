package com.allstate.crm.dao;

import com.allstate.crm.dao.common.DatabaseGenerateCounter;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InteractionDAOImpl implements InteractionDAO  {

    public InteractionDAOImpl()
    {

    }

    @Autowired
    private MongoTemplate mongoTemp;

    @Autowired
    private DatabaseGenerateCounter dbcounter;

    @Override
    public long saveInteraction(Interaction interaction) {
        if(interaction.getCustId() > 0) {
            interaction.setId(dbcounter.generateSequence(Interaction.SEQUENCE_NAME));
            Interaction interactionsRes = mongoTemp.insert(interaction);
            if (interactionsRes != null) {
                return interactionsRes.getId();
            } else {
                return 0;
            }
        }
        return 0;
    }

    @Override
    public List<Interaction> getInteractionDetailByCustId(int custId) {
        List<Interaction> interactionDetalils = null;
        Query detailQuery = new Query();
        detailQuery.addCriteria(Criteria.where("custId").is(custId));
        interactionDetalils = mongoTemp.find(detailQuery, Interaction.class);
        return interactionDetalils;
    }

}
