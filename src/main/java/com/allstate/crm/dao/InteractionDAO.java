package com.allstate.crm.dao;

import com.allstate.crm.entities.Interaction;

import java.util.List;

public interface InteractionDAO  {

    long saveInteraction(Interaction interaction);

    List<Interaction> getInteractionDetailByCustId(int custId);

}
