package com.allstate.crm.dao.common;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.DatabaseSequence;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Component
public class DatabaseGenerateCounter {

    public DatabaseGenerateCounter(){

    }

    @Autowired
    private MongoTemplate mongoOperations;

    public long generateSequence(String seqName) {
        DatabaseSequence counter = mongoOperations.findAndModify(Query.query( Criteria.where("_id").is(seqName)),
                new Update().inc("seq",1), FindAndModifyOptions.options().returnNew(true).upsert(true),
                DatabaseSequence.class);
        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }

    public long updateSequence(long counter,String seqName) {
        Query q = new Query();
        q.addCriteria(Criteria.where("_id").is(seqName));
        Update u = new Update();
        u.set("seq", counter);
        UpdateResult ur = mongoOperations.updateFirst(q,u, DatabaseSequence.class);
        return ur.getModifiedCount();
    }
}
