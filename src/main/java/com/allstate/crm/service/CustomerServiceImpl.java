package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDAO;
import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.exception.PolicyServiceException;
import com.allstate.crm.utility.InteractionAuditHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.sql.Array;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    public CustomerServiceImpl()
    {

    }

    @Autowired
    private CustomerDAO custdao;

    @Autowired
    private PolicyDAO policyDao;

    @Autowired
    AuditInteractionService auditService;

    @Override
    public long saveCustomerInfo(Customer customer, List<Policy> policies, Interaction interaction) {
        long custResult;
        int policyResult;
        String policyString = "";

        try {
            if (customer.getId() == 0) {
                custResult = custdao.saveCustomer(customer);
                if (custResult == 0) {
                    throw new PolicyServiceException("Error occured, record not saved.");
                }

                customer.setId(custResult);
                if (custResult > 0L && policies.size() > 0) {
                    policies.stream().forEach(x -> x.setCustId(custResult));
                    policyDao.savePolicy(policies);
                    List<String> policyList = policies.stream().map(x -> x.getPolicyNumber()).collect(Collectors.toList());
                    policyString = String.join(", ",policyList);
                }
                long interactionAuditRes = auditService.auditInteractions(
                        customer,
                        "Create customer request",
                        policyString,
                        null);
            } else {
                long interactionAuditRes = auditService.auditInteractions(
                         customer,
                        "Update customer request",
                        null,
                         customer.getLanguage()
                );
                custResult = custdao.updateCustomer(customer);
                if(custResult > 0 && policies.size() > 0) {
                    policyResult = policyDao.updatePolicy(policies.stream().filter(x -> x.getId() > 0 && x.getCustId() > 0).collect(Collectors.toList()));
                    policyDao.savePolicy(policies.stream().filter(x -> x.getId() == 0).collect(Collectors.toList()));
                }
            }
        }
        catch(Exception e)
        {
            throw new PolicyServiceException("Error occured, record not saved.");
        }

       return  custResult;
    }

    @Override
    public Customer getCustomerById(long id) {
        Customer customer = custdao.getCustomerById(id);
        return customer;
    }
}
