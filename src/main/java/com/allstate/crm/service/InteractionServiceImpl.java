package com.allstate.crm.service;

import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.exception.InteractionNotFound;
import com.allstate.crm.utility.InteractionAuditHelper;
import com.allstate.crm.utility.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class InteractionServiceImpl implements InteractionService  {

    public InteractionServiceImpl() {
    }

    @Autowired
    InteractionDAO interactionDao;


    @Autowired
    private ValidationHelper validation;

    @Override
    public List<Interaction> getInteractionDetailByCustId(int custId) {
        List<Interaction> interactionDetails = null;
        if(!validation.validateIntegerGreaterThanZero(custId))
        {
            throw new InteractionNotFound("Interaction Information not found.");
        }

        try {
            interactionDetails = interactionDao.getInteractionDetailByCustId(custId);
        }
        catch (Exception ex)
        {
            throw new InteractionNotFound("Error occured while processing your request.");
        }

        return interactionDetails;
    }

}
