package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDAO;
import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.dao.PolicyDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;
import com.allstate.crm.exception.PolicyNotFoundException;
import com.allstate.crm.exception.PolicyServiceException;
import com.allstate.crm.utility.InteractionAuditHelper;
import com.allstate.crm.utility.ValidationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PolicyServiceImpl implements PolicyService{

    @Autowired
    private PolicyDAO policyDao;

    @Autowired
    private ValidationHelper validation;

    @Autowired
    AuditInteractionService audit;

    public PolicyServiceImpl() {
    }

    @Override
    public List<Policy> getPolicyDetailByCustId(int custId) {
        if(!validation.validateIntegerGreaterThanZero(custId))
        {
            throw new PolicyServiceException("Policy Information not found.");
        }
        Customer cust = new Customer();
        cust.setId(custId);
        long res = audit.auditInteractions(cust, "View customer info", null, null);
        List<Policy> policyDetails = null;

        try {
            policyDetails = policyDao.getPolicyByCustomerId(custId);
        }
        catch (Exception ex)
        {
            throw new PolicyNotFoundException("Error occured while processing your request.");
        }

        return policyDetails;
    }
}
