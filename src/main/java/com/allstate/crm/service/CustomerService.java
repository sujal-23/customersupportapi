package com.allstate.crm.service;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.entities.Policy;

import java.util.List;

public interface CustomerService {

    long saveCustomerInfo(Customer customer, List<Policy> policies, Interaction interaction);

    Customer getCustomerById(long id);
}
