package com.allstate.crm.service;

import com.allstate.crm.dao.CustomerDAO;
import com.allstate.crm.dao.InteractionDAO;
import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.Interaction;
import com.allstate.crm.utility.InteractionAuditHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AuditInteractionServiceImpl implements AuditInteractionService {

    public AuditInteractionServiceImpl(){

    }

    @Autowired
    CustomerDAO customerDAO;

    @Autowired
    InteractionDAO interactionDao;

    @Autowired
    private InteractionAuditHelper auditInteraction;

    @Override
    public long auditInteractions(Customer customer, String auditType, String policy, String language) {

        Interaction interactionaudit = new Interaction();
        if (auditType.indexOf("Create") < 0) {
            System.out.println("in Audit" + customer);
            Customer cust = customerDAO.getCustomerById(customer.getId());
            System.out.println("call from cust dao " + cust );
            interactionaudit.setPhone(cust.getPhone());
            interactionaudit.setCreatedDate(new Date());
            interactionaudit.setCustId(cust.getId());
            interactionaudit.setCreatedBy("System User 1");
            interactionaudit.setNotes(auditInteraction.buildCreateAuditInteraction(
                    null,
                    auditType,
                    cust.getLastName() + ", " + cust.getFirstName(),
                    (language != null && cust.getLanguage().equals(language)) ? null : language,
                    cust.getLanguage()
            ));
        } else {
            interactionaudit.setPhone(customer.getPhone());
            interactionaudit.setCreatedDate(new Date());
            interactionaudit.setCustId(customer.getId());
            interactionaudit.setCreatedBy("System User 1");
            interactionaudit.setNotes(auditInteraction.buildCreateAuditInteraction(
                    policy,
                    auditType,
                    customer.getLastName() + ", " + customer.getFirstName(),
                    null,
                    null
            ));
        }

        return interactionDao.saveInteraction(interactionaudit);
    }
}
