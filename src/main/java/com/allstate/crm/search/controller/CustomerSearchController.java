package com.allstate.crm.search.controller;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.search.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/crm/customersearch")
@CrossOrigin(origins = "*")
public class CustomerSearchController {

    Logger logger = LoggerFactory.getLogger(CustomerSearchController.class);

    @Autowired
    SearchService searchService;


    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus() {
        logger.info("starting method status ");
        return "connected";
    }

    @RequestMapping(value = "/phonenumber/{phoneNumber}", method = RequestMethod.GET)
    public List<Customer> fetchCustomersByPhoneNumber(@PathVariable String phoneNumber) {
        logger.info("starting method fetchCustomersByPhoneNumber with phone Number as " + phoneNumber);
        List<Customer> myCustomers = searchService.getCustomersByPhoneNumber(phoneNumber);
        logger.info("customer details fetched :" + myCustomers);
        return myCustomers;
    }
    @RequestMapping(value = "/email/{email}", method = RequestMethod.GET)
    public List<Customer> fetchCustomersByEmail(@PathVariable String email) {
        logger.info("starting method fetchCustomersByEmail with email Number as " + email);
        List<Customer> myCustomers = searchService.getCustomersByEmail(email);
        logger.info("customer details fetched :" + myCustomers);
        return myCustomers;
    }
    @RequestMapping(value = "/policynumber/{policynumber}", method = RequestMethod.GET)
    public List<Customer> fetchCustomersByPolicyNumber(@PathVariable String policynumber) {
        logger.info("starting method fetchCustomersByPolicyNumber with policy Number as " + policynumber);
        List<Customer> myCustomers = searchService.getCustomersByPolicyNumber(policynumber);
        logger.info("customer details fetched :" + myCustomers);
        return myCustomers;
    }
    @RequestMapping(value = "/firstname/{firstname}", method = RequestMethod.GET)
    public List<Customer> fetchCustomersByFirstName(@PathVariable String firstname) {
        logger.info("starting method fetchCustomersByFirstName with firstname as  " + firstname);
        List<Customer> myCustomers = searchService.getCustomersByFirstName(firstname);
        logger.info("customer details fetched :" + myCustomers);
        return myCustomers;
    }

    @RequestMapping(value = "/name/{firstname}", method = RequestMethod.GET)
    public List<Customer> fetchCustomersByName(@PathVariable String firstname) {
        logger.info("starting method fetchCustomersByFirstName with firstname as  " + firstname);
        List<Customer> myCustomers = searchService.getCustomersByFirstName(firstname);
        logger.info("customer details fetched :" + myCustomers);
        return myCustomers;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public int addPayment(@RequestBody Customer customer) {
        logger.info("starting method addPayment ");
        int myCustomer = searchService.saveCustomer(customer);
        logger.info("payment details added : " + myCustomer);
        return myCustomer;
    }

}

