package com.allstate.crm.entities;

import com.allstate.crm.utility.DateFormatDeSerializerHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Document
public class Policy {
    @Id
    private long id;

    @Transient
    public static final String SEQUENCE_NAME = "policy_sequence";

    private long custId;
    @NotEmpty(message="Enter Valid policy number")
    @Pattern(regexp="^[a-zA-Z0-9]+$", message="not valid policy number")
    @Size(max = 10)
    private String policyNumber;

    private Double premiumAmount;

    @JsonDeserialize(using = DateFormatDeSerializerHandler.class)
    private Date effectiveDate;

    @NotEmpty(message = "enter product type")
    @Pattern(regexp="^[a-zA-Z0-9]+$", message="not valid product type")
    @Size(max = 10)
    private String productType;

    @NotEmpty(message = "enter valid company")
    @Pattern(regexp="^[A-Za-z0-9'\\.\\-\\s\\,]+$", message="not valid company")
    @Size(max = 20)
    private String company;

    @Size(max=20)
    @NotEmpty(message = "enter status")
    @Pattern(regexp="^[a-zA-Z]+$", message="not valid status")
    private String status;

    @JsonDeserialize(using = DateFormatDeSerializerHandler.class)
    private Date terminationDate;

    private String terminationReason;

    @Autowired
    private ObjectMapper objectMapper;
    public Policy()
    {

    }
    public Policy(long id, long custId, String policyNumber, Double premiumAmount, Date effectiveDate, String productType, String company, String status, Date terminationDate, String terminationReason) {
        this.id = id;
        this.custId = custId;
        this.policyNumber = policyNumber;
        this.premiumAmount = premiumAmount;
        this.effectiveDate = effectiveDate;
        this.productType = productType;
        this.company = company;
        this.status = status;
        this.terminationDate = terminationDate;
        this.terminationReason = terminationReason;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public Double getPremiumAmount() {
        return premiumAmount;
    }

    public void setPremiumAmount(Double premiumAmount) {
        this.premiumAmount = premiumAmount;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getTerminationReason() {
        return terminationReason;
    }

    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }
}
