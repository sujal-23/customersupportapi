package com.allstate.crm.entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.util.List;

@Document
public class Customer {
    @Transient
    public static final String SEQUENCE_NAME = "customer_sequence";
    @Id
    private long id;


    @NotEmpty(message="Enter Valid first name")
    @Pattern(regexp="^[a-zA-Z0-9]+$", message="not valid first name")
    @Size(max = 20)
    private String firstName;


    @NotEmpty(message="Enter Valid last name")
    @Pattern(regexp="^[a-zA-Z0-9]+$", message="not valid last name")
    @Size(max = 20)
    private String lastName;

    @Min(1)
    @Max(100)
    private int age;

    @NotEmpty(message="Enter Valid address")
    @Size(max = 50)
    @Pattern(regexp="^[A-Za-z0-9'\\.\\-\\s\\,]+$", message="not valid Address")
    private String address;

    @NotEmpty(message="Enter Valid city")
    @Size(max = 20)
    @Pattern(regexp="^[a-zA-Z]+$", message="not valid city")
    private String city;

    @NotEmpty(message="Enter Valid state")
    @Pattern(regexp="^[a-zA-Z]+$", message="not valid state")
    @Size(max = 20)
    private String state;

    @NotEmpty(message="Enter Valid phone")
    @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Mobile number is invalid")
    private String phone;

    @NotEmpty(message="Enter Valid email")
    @Email
    private String email;

    @NotEmpty(message="Enter Valid gender")
    @Pattern(regexp="^[a-zA-Z]+$", message="not valid gender")
    @Size(max = 5)
    private String gender;

    //@Pattern(regexp="^(?!666|000|9\\d{2})\\d{3}-(?!00)\\d{2}-(?!0{4})\\d{4}$", message="not valid ssn")
    private String ssn;

    @NotEmpty(message="Enter Valid language")
    @Size(max = 10)
    @Pattern(regexp="^[a-zA-Z]+$", message="not valid language")
    private String language;

    private List<String> policies;

    public Customer()
    {

    }

    public Customer(long id, String firstName, String lastName, int age, String address, String city, String state, String phone, String email, String gender, String ssn, String language) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
        this.ssn = ssn;
        this.language = language;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getPolicies() { return policies; }

    public void setPolicies(List<String> policies) { this.policies = policies; }


}
